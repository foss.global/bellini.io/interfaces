import * as plugins from './bellini-interfaces.plugins';

export interface IBelliniImage {
  dimensions: string;
  crop: string;
  urlSmall: string;
  urlMedium: string;
  urlLarge: string;
}

export interface IBelliniArticle extends plugins.tsclass.content.IArticle {
  url: string;
  timestamp: number;
  title: string;
  author: IBelliniAuthor;
  belliniId: string;
  htmlArticle: string;
  featuredImageUrl: string;
  images: IBelliniImage[];
  visibility: 'public' | 'private';
}

export interface IBelliniAuthor extends plugins.tsclass.content.IAuthor {
  name: string;
  image: IBelliniImage;
  belliniId: string;
}
