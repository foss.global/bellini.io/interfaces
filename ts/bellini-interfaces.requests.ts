import * as plugins from './bellini-interfaces.plugins';
import * as objectInterfaces from './bellini-interfaces.objects';

// tslint:disable-next-line: class-name
export interface IRequest_Any_Bellini_GetArticles
  extends plugins.typedrequestInterfaces.implementsTR<
    plugins.typedrequestInterfaces.ITypedRequest,
    IRequest_Any_Bellini_GetArticles
  > {
  method: 'getArticles';
  request: {
    publicationName: string;
    from: number;
    to: number;
  };
  response: {
    articles: objectInterfaces.IBelliniArticle[];
  };
}

// tslint:disable-next-line: class-name
export interface IRequest_Any_Bellini_GetAuthors
  extends plugins.typedrequestInterfaces.implementsTR<
    plugins.typedrequestInterfaces.ITypedRequest,
    IRequest_Any_Bellini_GetAuthors
  > {
  method: 'getAuthors';
  request: {
    publicationName: string;
  };
  response: {
    authors: objectInterfaces.IBelliniAuthor[];
  };
}
